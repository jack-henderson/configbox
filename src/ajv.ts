import Ajv from 'ajv';
import addFormats from 'ajv-formats';

// https://github.com/sinclairzx81/typebox#ajv
export const makeAjv = (): Ajv => (
	addFormats(new Ajv({
		allErrors: true,
	}), [
		'date-time',
		'time',
		'date',
		'email',
		'hostname',
		'ipv4',
		'ipv6',
		'uri',
		'uri-reference',
		'uuid',
		'uri-template',
		'json-pointer',
		'relative-json-pointer',
		'regex',
	])
);