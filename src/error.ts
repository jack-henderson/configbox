export class ConfigurationError extends Error {
	constructor(errors: string[]) {
		super('Invalid configuration:' + ConfigurationError.formatErrors(errors));
		this.name = 'ConfigurationError';
	}

	static formatErrors(errors: string[]): string {
		if(errors.length === 1) {
			return ' ' + errors[0]!.trim(); // eslint-disable-line @typescript-eslint/no-non-null-assertion
		}

		return '\n' + errors.map(err => ' - ' + err.trim()).join('\n');
	}
}