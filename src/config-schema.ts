/* c8 ignore start */
import type { TBoolean, TInteger, TNumber, TString, TArray, TLiteral, TObject, TUnion } from '@sinclair/typebox';


export type ConfigBoxScalar = (
	| TBoolean
	| TInteger
	| TNumber
	| TString
);
export type ConfigBoxValue = (
	| ConfigBoxScalar
	| TArray<ConfigBoxScalar>
	| TLiteral
	| TUnion<TLiteral<string>[]>
);
export type ConfigBoxObject = TObject<Record<string, ConfigBoxValue>>;
export type ConfigBoxSchema = ConfigBoxObject;