import type { ConversionContext, ConversionError, Converters } from './converter';
import type { ConfigBoxValue } from './config-schema';


const parseBoolean = (name: string, value: string, error: ConversionContext<ConfigBoxValue>['error']): boolean | ConversionError => {
	switch(value.toLowerCase()) {
		case '0':
		case 'false':
			return false;
		case '1':
		case 'true':
			return true;
		default:
			return error(`${name} must be boolean; use one of "0", "1", "true", or "false"`);
	}
};

const integerRegex = /^-?[0-9]*$/g;
const numberRegex = /^-?[0-9]*\.?[0-9]*$/g;

export const CONVERTERS: Converters = {
	Array: ({ name, schema, value, convert }) => (
		value
			.split(',')
			.map((item, index) => convert(`${name}/${index}`, schema.items as any, item.trim()) as any) // eslint-disable-line @typescript-eslint/no-explicit-any
	),
	Boolean: ({ name, value, error }) => parseBoolean(name, value, error),
	Integer: ({ name, value, error }) => {
		if(!value.match(integerRegex)) {
			return error(`${name} must be integer`);
		}

		return Number.parseInt(value, 10);
	},
	Literal: ({ name, schema, value, error }) => {
		switch(typeof(schema.const)) {
			case 'boolean':
				return parseBoolean(name, value, error);
			case 'number':
				return Number.parseFloat(value);
			default:
				return value;
		}
	},
	Number: ({ name, value, error }) => {
		if(!value.match(numberRegex)) {
			return error(`${name} must be number`);
		}
		return Number.parseFloat(value);
	},
	String: ({ value }) => value,
	Union: ({ value }) => value,
};
