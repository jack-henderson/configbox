import type { Static } from '@sinclair/typebox';
import type { ConfigBoxValue } from './config-schema';
import { Kind } from '@sinclair/typebox';
import { CONVERTERS } from './converters';


export class ConversionError {
	readonly message: string;

	constructor(message: string) {
		this.message = message;
	}
}

/**
 * Context passed into a {@link ConversionFunction}.
 */
export class ConversionContext<Schema extends ConfigBoxValue> {
	/**
	 * The name of the property being converted.
	 */
	readonly name: string;
	/**
	 * The schema describing the property being converted.
	 */
	readonly schema: Schema;
	/**
	 * The value of the property being converted.
	 */
	readonly value: string;

	/**
	 * Create a `ConversionError` for this field. Return this from a
	 * {@link ConversionFunction} to raise a `ConfigurationError` for the
	 * property.
	 *
	 *```ts
	 * ({ name, error }) => error(`${name} could not be converted!`);
	 * ```
	 */
	readonly error: (message: string) => ConversionError;

	readonly convert: <S extends ConfigBoxValue>(name: string, schema: Schema, value: string) => Static<S>;

	constructor(name: string, schema: Schema, value: string, converter: Converter) {
		this.name = name;
		this.schema = schema;
		this.value = value;
		this.error = message => new ConversionError(message);
		this.convert = (name, schema, value) => converter.convert(name, schema, value) as any; // eslint-disable-line @typescript-eslint/no-explicit-any

		Object.freeze(this);
	}
}

/**
 * Function which converts the `string` value provided in the environment into
 * an appropriate value for a given schema kind.
 *
 * @template Schema The schema describing the value being converted.
 * @returns The converted value, or a `ConversionError` created by
 * 	{@link ConversionContext.error()}.
 */
type ConversionFunction<Schema extends ConfigBoxValue> = (context: ConversionContext<Schema>) => Static<Schema> | ConversionError;

export type Converters = {
	[ Schema in ConfigBoxValue as Schema[typeof Kind]]: ConversionFunction<Schema>;
};

export class Converter {
	private readonly converters: Readonly<Converters>;
	private readonly errors: Record<string, string | undefined> = {};

	constructor() {
		this.converters = CONVERTERS;
	}

	convert<Schema extends ConfigBoxValue>(name: string, schema: Schema, value: string | undefined | null): Static<Schema> | undefined {
		const converter = this.converters[schema[Kind]] as unknown as ConversionFunction<Schema>;
		if(!converter) {
			throw new TypeError(`Cannot convert property "${name}" of kind "${schema[Kind]}"`);
		}

		if(value === undefined || value === null) {
			return schema.default;
		}

		const result = converter(new ConversionContext(name, schema, value, this));
		if(result instanceof ConversionError) {
			this.errors[name] = result.message;
			return undefined;
		}

		return result;
	}

	consumeError(propertyName: string): string | undefined {
		const error = this.errors[propertyName];
		if(error) {
			this.errors[propertyName] = undefined;
		}

		return error;
	}
}
