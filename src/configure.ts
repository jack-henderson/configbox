import type { Static } from '@sinclair/typebox';
import type { ConfigBoxSchema } from './config-schema';
import { makeAjv } from './ajv';
import { Converter } from './converter';
import { Reporter } from './reporter';


export const configure = <Schema extends ConfigBoxSchema>(schema: Schema, environment: Readonly<Record<string, string>>): Static<Schema> => {
	if(schema.type !== 'object') {
		throw new TypeError(`Expected a schema of type "object", but got "${schema.type}"`);
	}

	const converter = new Converter();
	const converted: Record<string, unknown> = {};
	for(const propKey in schema.properties) {
		const propSchema = schema.properties[propKey]!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
		const propValue = environment[propKey];
		converted[propKey] = converter.convert(propKey, propSchema, propValue);
	}

	const ajv = makeAjv();
	const validator = ajv.compile(schema);
	const valid = validator(converted);

	if(valid) {
		return converted as Static<Schema>;
	}

	const reporter = new Reporter(converter, validator.errors!); // eslint-disable-line @typescript-eslint/no-non-null-assertion
	return reporter.report();
};