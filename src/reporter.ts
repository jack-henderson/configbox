import type { ErrorObject } from 'ajv';
import type { Converter } from './converter';
import { ConfigurationError } from './error';


export class Reporter {
	private readonly converter: Converter;
	private readonly errors: ErrorObject[];
	private readonly messages: string[] = [];

	constructor(converter: Converter, errors: readonly ErrorObject[]) {
		this.converter = converter;
		this.errors = [ ...errors ];
	}


	report(): never {
		while(this.errors.length) {
			const error = this.errors.shift()!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
			this.messages.push(this.reportError(error));
		}

		throw new ConfigurationError(this.messages);
	}

	private reportError(error: ErrorObject): string {
		const propertyName = error.instancePath.slice(1);
		switch(error.keyword) {
			case 'required': {
				/*
					If we have a conversion error for a missing property, that means that
					environment did contain a value for that property, but the converter
					failed to convert it.
				*/
				const conversionError = this.converter.consumeError(error.params['missingProperty']);
				if(conversionError) {
					return conversionError;
				}
				break;
			}
			case 'const': {
				const relatedErrors: ErrorObject[] = [ error ];
				while(this.errors[0]?.instancePath === error.instancePath) {
					const relatedError = this.errors.shift()!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
					if(relatedError.keyword === 'const') {
						relatedErrors.push(relatedError);
					}
				}

				const allowedValues = relatedErrors.map(r => {
					const allowedValue = r.params['allowedValue'];
					if(typeof(allowedValue) === 'string') {
						return `"${allowedValue}"`;
					}
					return allowedValue;
				});

				if(allowedValues.length === 1) {
					return `${propertyName} must be equal to ${allowedValues[0]}`;
				}

				return `${propertyName} must be one of ${allowedValues.join(', ')}`;
			}
		}

		return propertyName + ' ' + error.message!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
	}
}