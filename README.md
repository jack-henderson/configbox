**ConfigBox**
=============

Application configuration using [TypeBox](https://github.com/sinclairzx81/typebox#readme)
and [Ajv](https://github.com/ajv-validator/ajv#readme) 

------------
Installation
------------
```bash
yarn add @sinclair/typebox configbox
# or
npm i @sinclair/typebox configbox
# etc.
```