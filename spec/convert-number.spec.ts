import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { configure } from '../src';


describe('convert number', () => {
	const schema = Type.Object({
		FOO: Type.Number({ maximum: 10 }),
	});

	it('throws for non-numeric value', () => {
		expect(() => configure(schema, { FOO: 'not a number' }))
			.to.throw('FOO must be number');
	});

	it('throws for invalid numeric value', () => {
		expect(() => configure(schema, { FOO: '1.2.3' }))
			.to.throw('FOO must be number');
	});

	it('converts decimal value', () => {
		expect(configure(schema, { FOO: '5.5' }))
			.to.deep.eq({
				FOO: 5.5,
			});
	});

	it('converts negative value', () => {
		expect(configure(schema, { FOO: '-5' }))
			.to.deep.eq({
				FOO: -5,
			});
	});

	it('converts decimal without leading number', () => {
		expect(configure(schema, { FOO: '.5' }))
			.to.deep.eq({
				FOO: 0.5,
			});
	});

	it('converts integer value', () => {
		expect(configure(schema, { FOO: '5' }))
			.to.deep.eq({
				FOO: 5,
			});
	});
});