import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { configure } from '../src';


describe('convert integer', () => {
	const schema = Type.Object({
		FOO: Type.Integer(),
	});

	it('throws for non-numeric value', () => {
		expect(() => configure(schema, { FOO: 'not a number' }))
			.to.throw('FOO must be integer');
	});

	it('throws for invalid numeric value', () => {
		expect(() => configure(schema, { FOO: '5not a number' }))
			.to.throw('FOO must be integer');
	});

	it('throws for decimal value', () => {
		expect(() => configure(schema, { FOO: '5.5' }))
			.to.throw('FOO must be integer');
	});

	it('converts negative value', () => {
		expect(configure(schema, { FOO: '-5' }))
			.to.deep.eq({
				FOO: -5,
			});
	});

	it('converts value to a number', () => {
		const result = configure(schema, { FOO: '5' });

		expect(result)
			.to.deep.eq({
				FOO: 5,
			});
	});
});