import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('convert union', () => {
	it('converts union of types', () => {
		const schema = Type.Object({
			FOO: Type.Union([
				Type.Literal('One'),
				Type.Literal('Two'),
			]),
		});

		expect(configure(schema, {
			FOO: 'Two',
		}))
			.to.deep.eq({
				FOO: 'Two',
			});
	});

	it('throws ConfigurationError if the value does not match', () => {
		const schema = Type.Object({
			FOO: Type.Union([
				Type.Literal('Valid'),
				Type.Literal('OtherValid'),
			]),
		});

		expect(() => configure(schema, { FOO: 'invalid' }))
			.to.throw(ConfigurationError, 'FOO must be one of "Valid", "OtherValid"');
	});
});