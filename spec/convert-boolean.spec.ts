import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('convert boolean', () => {
	const schema = Type.Object({
		FOO: Type.Boolean(),
	});

	it('throws ConfigurationError for invalid value', () => {
		expect(() => configure(schema, { FOO: 'not a boolean' }))
			.to.throw(ConfigurationError, 'FOO must be boolean; use one of "0", "1", "true", or "false"');
	});

	Object.entries({
		'0': false,
		'1': true,
		'false': false,
		'true': true,
	}).forEach(([ FOO, expectation ]) => {
		it(`converts "${FOO}" into ${expectation}`, () => {
			expect(configure(schema, { FOO }))
				.to.deep.eq({
					FOO: expectation,
				});
		});
	});
});