import type { ConfigBoxSchema } from '../src';
import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('configure()', () => {
	it('throws if passed a non-object schema', () => {
		const schema = Type.Array(Type.String());

		// @ts-expect-error Invalid config schema.
		expect(() => configure(schema))
			.to.throw(TypeError, 'Expected a schema of type "object", but got "array"');
	});

	it('throws TypeError if there is no converter of a schema kind', () => {
		const schema = Type.Object({
			FOO: Type.Null(),
		});

		// @ts-expect-error Invalid config value.
		expect(() => configure(schema, { FOO: '' }))
			.to.throw(TypeError, 'Cannot convert property "FOO" of kind "Null"');
	});

	it('throws TypeError if there is no converter of a schema kind for an optional property', () => {
		const schema = Type.Object({
			FOO: Type.Optional(Type.Null()),
		});

		// @ts-expect-error Invalid config value.
		expect(() => configure(schema, {}))
			.to.throw(TypeError, 'Cannot convert property "FOO" of kind "Null"');
	});

	it('does not include properties not present in schema', () => {
		const schema = Type.Object({
			FOO: Type.String(),
		});

		expect(configure(schema, { FOO: 'foo', BAR: 'bar' }))
			.to.deep.eq({
				FOO: 'foo',
			});
	});

	it('uses default value if present', () => {
		const schema = Type.Object({
			FOO: Type.String({ default: 'default foo!' }),
		});

		expect(configure(schema, {}))
			.to.deep.eq({
				FOO: 'default foo!',
			});
	});

	describe('complex configuration', () => {
		const schema = Type.Object({
			API_URL: Type.String({ format: 'uri' }),
			DATABASE_CONNECTION: Type.String({ format: 'uri', pattern: '^db://' }),
			NUM_ITEMS: Type.Integer({ minimum: 0, maximum: 10 }),
			ADMIN_EMAIL: Type.String({ format: 'email' }),
			ENVIRONMENT: Type.Union([
				Type.Literal('dev'),
				Type.Literal('prod'),
			]),
			ENABLE_FEATURE: Type.Boolean(),
			ALLOWED_IPS: Type.Array(Type.String({ format: 'ipv4' }), { minItems: 2 }),
			AMOUNT: Type.Number(),
		}) satisfies ConfigBoxSchema;

		it('fails for multiple items', () => {
			try {
				configure(schema, {
					API_URL: 'not a URI',
					DATABASE_CONNECTION: 'https://not-the-db.example.com',
					NUM_ITEMS: '-1',
					ENVIRONMENT: 'nope',
					ENABLE_FEATURE: 'maybe',
					ALLOWED_IPS: '127.0.0.1',
					AMOUNT: '1.2.3',
				});
				assert('Should have thrown.');
			} catch(err: any) { // eslint-disable-line @typescript-eslint/no-explicit-any
				expect(err)
					.to.be.instanceof(ConfigurationError);

				[
					'API_URL must match format "uri"',
					'DATABASE_CONNECTION must match pattern "^db://',
					'NUM_ITEMS must be >= 0',
					'must have required property \'ADMIN_EMAIL\'',
					'ENVIRONMENT must be one of "dev", "prod"',
					'ENABLE_FEATURE must be boolean',
					'ALLOWED_IPS must NOT have fewer than 2 items',
					'AMOUNT must be number',
				].forEach(expectedMessage => {
					expect(err.message)
						.to.include(' - ' + expectedMessage);
				});
			}
		});

		it('succeeds for valid environment', () => {
			expect(configure(schema, {
				API_URL: 'https://example.com/api',
				DATABASE_CONNECTION: 'db://example.com/db',
				NUM_ITEMS: '5',
				ADMIN_EMAIL: 'admin@example.com',
				ENVIRONMENT: 'dev',
				ENABLE_FEATURE: 'true',
				ALLOWED_IPS: '127.0.0.1, 192.168.0.1',
				AMOUNT: '1.23',
			}))
				.to.deep.eq({
					API_URL: 'https://example.com/api',
					DATABASE_CONNECTION: 'db://example.com/db',
					NUM_ITEMS: 5,
					ADMIN_EMAIL: 'admin@example.com',
					ENVIRONMENT: 'dev',
					ENABLE_FEATURE: true,
					ALLOWED_IPS: [ '127.0.0.1', '192.168.0.1' ],
					AMOUNT: 1.23,
				});
		});
	});
});