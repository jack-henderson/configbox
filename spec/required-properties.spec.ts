import type { ConfigBoxSchema } from '../src';
import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('required properties', () => {
	it('throws for a missing required property', () => {
		const schema = Type.Object({
			FOO: Type.String(),
		});

		expect(() => configure(schema, {}))
			.to.throw(ConfigurationError, 'Invalid configuration: must have required property \'FOO\'');
	});

	it('throws for multiple missing required properties', () => {
		const schema = Type.Object({
			FOO: Type.String(),
			BAR: Type.String(),
		});

		expect(() => configure(schema, {}))
			.to.throw(ConfigurationError, 'Invalid configuration:\n - must have required property \'FOO\'\n - must have required property \'BAR\'');
	});

	it('does not throw for missing optional properties', () => {
		const schema = Type.Object({
			FOO: Type.String(),
			BAR: Type.Optional(Type.String()),
		});

		expect(() => configure(schema, { FOO: 'foo' }))
			.to.not.throw();
	});

	it('returns configuration object for valid environment', () => {
		const schema = Type.Object({
			FOO: Type.String(),
			BAR: Type.String(),
		}) satisfies ConfigBoxSchema;

		expect(configure(schema, { FOO: 'FOO', BAR: 'BAR' }))
			.to.deep.eq({
				FOO: 'FOO',
				BAR: 'BAR',
			});
	});
});