import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('convert array', () => {
	const schema = Type.Object({
		FOO: Type.Array(Type.Integer()),
	});

	it('converts an array of convertible values', () => {
		expect(configure(schema, { FOO: '1, 2, 3' }))
			.to.deep.eq({
				FOO: [ 1, 2, 3 ],
			});
	});

	it('converts array of string patterns', () => {
		const schema = Type.Object({
			FOO: Type.Array(Type.String({ pattern: '^foo' })),
		});

		expect(configure(schema, {
			FOO: 'foo, foo, foo',
		}))
			.to.deep.eq({
				FOO: [ 'foo', 'foo', 'foo' ],
			});

	});

	it('throws ConfigurationError if items do not pass validation', () => {
		expect(() => configure(schema, { FOO: '1, 2, three' }))
			.to.throw(ConfigurationError, 'FOO/2 must be integer');
	});

	it('throws ConfigurationError if items cannot be converted', () => {
		const badSchema = Type.Object({
			FOO: Type.Array(Type.Null()),
		});

		// @ts-expect-error Invalid schema.
		expect(() => configure(badSchema, { FOO: '1, 2, 3' }))
			.to.throw(TypeError, 'Cannot convert property "FOO/0" of kind "Null"');
	});
});