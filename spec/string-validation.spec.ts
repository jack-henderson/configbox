import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('string validation', () => {
	describe('pattern', () => {
		const schema = Type.Object({
			FOO: Type.String({ pattern: '^FOO' }),
		});

		it('throws for invalid pattern', () => {
			expect(() => configure(schema, { FOO: 'not FOO' }))
				.to.throw(ConfigurationError, 'FOO must match pattern "^FOO"');
		});

		it('accepts valid pattern', () => {
			expect(configure(schema, { FOO: 'FOO is valid' }))
				.to.deep.eq({
					FOO: 'FOO is valid',
				});
		});
	});

	describe('format', () => {
		const schema = Type.Object({
			FOO: Type.String({ format: 'uri' }),
		});

		it('throws for invalid format', () => {
			expect(() => configure(schema, { FOO: 'not a URL' }))
				.to.throw(ConfigurationError, 'FOO must match format "uri"');
		});

		it('accepts valid pattern', () => {
			expect(configure(schema, { FOO: 'https://example.com' }))
				.to.deep.eq({
					FOO: 'https://example.com',
				});
		});
	});
});