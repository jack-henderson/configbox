import { Type } from '@sinclair/typebox';
import { describe, expect, it } from 'vitest';
import { ConfigurationError, configure } from '../src';


describe('convert literal', () => {
	Object.entries({
		'0': false,
		'false': false,
		'1': true,
		'true': true,
	}).forEach(([ FOO, expectation ]) => {
		it(`converts a literal boolean "${FOO}" to ${expectation}`, () => {
			const schema = Type.Object({
				FOO: Type.Literal(expectation),
			});

			expect(configure(schema, { FOO }))
				.to.deep.eq({ FOO: expectation });
		});
	});

	it('converts a literal number', () => {
		const schema = Type.Object({
			FOO: Type.Literal(5),
		});

		expect(configure(schema, { FOO: '5' }))
			.to.deep.eq({
				FOO: 5,
			});
	});

	it('converts a literal string', () => {
		const schema = Type.Object({
			FOO: Type.Literal('foo'),
		});

		expect(configure(schema, { FOO: 'foo' }))
			.to.deep.eq({
				FOO: 'foo',
			});
	});

	it('throws ConfigurationError if string does not validate', () => {
		const schema = Type.Object({
			FOO: Type.Literal('foo'),
		});

		expect(() => configure(schema, { FOO: 'bar' }))
			.to.throw(ConfigurationError, 'FOO must be equal to "foo"');
	});

	it('throws ConfigurationError if number does not validate', () => {
		const schema = Type.Object({
			FOO: Type.Literal(5),
		});

		expect(() => configure(schema, { FOO: '6' }))
			.to.throw(ConfigurationError, 'FOO must be equal to 5');
	});
});