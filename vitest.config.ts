/* eslint-disable import/no-unresolved */
import { defineConfig } from 'vitest/config';


export default defineConfig({
  test: {
		outputFile: {
			junit: 'spec-report.xml',
		},
		coverage: {
			enabled: true,
			reporter: [ 'text-summary', 'html' ],
			reportsDirectory: './.coverage',
			all: true,
			include: [ 'src/**/*' ],
			'100': true,
		},
  },
});